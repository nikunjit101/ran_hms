# Laravel

## Installation

setup .env & follow the following steps

```bash
composer install
```

```bash
npm install
```


```bash
npm run dev
```

```bash
php artisan migrate
```

```bash
php artisan serve
```
