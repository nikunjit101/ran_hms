<?php

namespace App\DataTables;

use App\Models\Room;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class RoomDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'rooms.datatables_actions')
            ->editColumn('room_type', function($data)
            {
                return $data->roomType->name;
            })
            ->editColumn('hotel', function($data)
            {
                return $data->hotel->name;
            })
            ->editColumn('smoke_friendly', function($data)
            {
                return $data->smoke_friendly == true ? 'Yes' : 'No';
            })
            ->editColumn('pet_friendly', function($data)
            {
                return $data->pet_friendly == 0 ? 'No' : 'Yes';
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Room $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Room $model)
    {
        if(Auth::user()->user_type == 'manager')
            return $model->newQuery()->whereHotel_id(Auth::user()->hotel_id)->with(['hotel','roomType']);
        else
            return $model->newQuery()->with(['hotel','roomType']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'frtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
//                'buttons'   => [
//                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
//                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
//                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
//                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
//                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
//                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name',
            'room_type',
            'hotel',
            'cost',
            'smoke_friendly',
            'pet_friendly'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'rooms_datatable_' . time();
    }
}
