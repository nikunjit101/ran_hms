<?php

namespace App\DataTables;

use App\Models\Task;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class TaskDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'tasks.datatables_actions')
            ->editColumn('hotel_id', function($data)
            {
                return $data->hotel->name;
            })
            ->editColumn('room_id', function($data)
            {
                return $data->room->name;
            })
            ->editColumn('assigned_to', function($data)
            {
                return $data->user->name;
            })
            ->editColumn('type_id', function($data)
            {
                return $data->taskType->name;
            })
            ->editColumn('status', function($data)
            {
                return $data->status;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Task $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Task $model)
    {
        if(Auth::user()->user_type == 'manager')
            return $model->newQuery()->whereHotel_id(Auth::user()->hotel_id)->with(['hotel','user']);
        if(Auth::user()->user_type == 'staff')
            return $model->newQuery()->whereAssigned_to(Auth::user()->id)->with(['hotel','user']);
        else
            return $model->newQuery()->with(['hotel','user']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'frtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
//                'buttons'   => [
//                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
//                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
//                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
//                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
//                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
//                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'hotel_id',
            'type_id',
            'task_date',
            'room_id',
            'assigned_to',
            'status'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'tasks_datatable_' . time();
    }
}
