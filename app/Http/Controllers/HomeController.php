<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()){
            if(Auth::user()->user_type == 'customer')
                return redirect(route('bookings.index'));
            elseif (Auth::user()->user_type == 'staff')
                return redirect(route('tasks.index'));
            elseif (Auth::user()->user_type == 'manager')
                return redirect(route('bookings.index'));
            else
                return redirect(route('users.index'));
        }
        return view('home');
    }
}
