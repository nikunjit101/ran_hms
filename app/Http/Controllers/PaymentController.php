<?php

namespace App\Http\Controllers;

use App\DataTables\PaymentDataTable;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreatePaymentRequest;
use App\Http\Requests\UpdatePaymentRequest;
use App\Models\Booking;
use App\Models\HotelBranch;
use App\Models\Payment;
use App\Repositories\PaymentRepository;
use Flash;
use Illuminate\Support\Facades\Auth;
use Response;

class PaymentController extends AppBaseController
{
    /** @var PaymentRepository $paymentRepository*/
    private $paymentRepository;

    public function __construct(PaymentRepository $paymentRepo)
    {
        $this->paymentRepository = $paymentRepo;
    }

    /**
     * Display a listing of the Payment.
     *
     * @param PaymentDataTable $paymentDataTable
     *
     * @return Response
     */
    public function index(PaymentDataTable $paymentDataTable)
    {
        if(Auth::user()->user_type == 'customer'){
            $data = [
                'payments' => Payment::orderBy('created_at')->with(['booking'])
                    ->get()
            ];
            return view('payments.customer_payment_list')->with($data);
        }
        else{
            $data = [
                'hotels' => HotelBranch::orderBy('name')->get()->pluck('name', 'id')
            ];
            return $paymentDataTable->render('payments.report')->with($data);
        }

    }


    /**
     * Show the form for creating a new Payment.
     *
     * @return Response
     */
    public function create()
    {
        $bookings = Booking::get()->pluck( 'id');
        
        return view('payments.create',['bookings'=>$bookings]);
    }

    /**
     * Store a newly created Payment in storage.
     *
     * @param CreatePaymentRequest $request
     *
     * @return Response
     */
    public function store(CreatePaymentRequest $request)
    {
        $input = $request->all();

        $payment = $this->paymentRepository->create($input);

        Flash::success('Payment saved successfully.');

        return redirect(route('payments.index'));
    }

    /**
     * Display the specified Payment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            Flash::error('Payment not found');

            return redirect(route('payments.index'));
        }

        return view('payments.show')->with('payment', $payment);
    }

    /**
     * Show the form for editing the specified Payment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            Flash::error('Payment not found');

            return redirect(route('payments.index'));
        }

        $bookings = Booking::get()->pluck('id');


        return view('payments.edit',['bookings'=>$bookings])->with('payment', $payment);
    }

    /**
     * Update the specified Payment in storage.
     *
     * @param int $id
     * @param UpdatePaymentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaymentRequest $request)
    {
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            Flash::error('Payment not found');

            return redirect(route('payments.index'));
        }

        $payment = $this->paymentRepository->update($request->all(), $id);

        Flash::success('Payment updated successfully.');

        return redirect(route('payments.index'));
    }

    /**
     * Remove the specified Payment from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            Flash::error('Payment not found');

            return redirect(route('payments.index'));
        }

        $this->paymentRepository->delete($id);

        Flash::success('Payment deleted successfully.');

        return redirect(route('payments.index'));
    }


    public function generatereport($start_date, $end_date ,$hotel_id)
    {
        $bookings = Booking::whereHotel_id($hotel_id)->with(['room','hotel','user'])->get();
        $filter_booking = [];
        foreach ($bookings as $booking){
            if((date($booking->check_in_date) > date($start_date)
                    &&
                    date($booking->check_in_date) < date($end_date))
                ||
                (date($booking->check_out_date) >  date($start_date)
                    &&
                    date($booking->check_out_date) < date($end_date))
            ){
                array_push($filter_booking,$booking);
            }
        }
        $data = [
            'bookings'=> $filter_booking
        ];
        return response()->json($data);
    }
}
