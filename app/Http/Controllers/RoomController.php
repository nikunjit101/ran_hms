<?php

namespace App\Http\Controllers;

use App\DataTables\RoomDataTable;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateRoomRequest;
use App\Http\Requests\UpdateRoomRequest;
use App\Models\HotelBranch;
use App\Models\RoomType;
use App\Repositories\RoomRepository;
use Flash;
use Illuminate\Http\UploadedFile;
use Response;

class RoomController extends AppBaseController
{
    /** @var RoomRepository $roomRepository*/
    private $roomRepository;

    public function __construct(RoomRepository $roomRepo)
    {
        $this->roomRepository = $roomRepo;
    }

    /**
     *
     * @param RoomDataTable $roomDataTable
     *
     * @return Response
     */
    public function index(RoomDataTable $roomDataTable)
    {
        return $roomDataTable->render('rooms.index');
    }

    /**
     *
     *
     * @return Response
     */
    public function create()
    {
        $roomTypes = RoomType::orderBy('name')->get()->pluck('name', 'id');
        $hotels = HotelBranch::orderBy('name')->get()->pluck('name', 'id');
        return view('rooms.create', ['roomTypes' => $roomTypes, 'hotels' => $hotels]);
    }

    /**
     *
     * @param CreateRoomRequest $request
     *
     * @return Response
     */
    public function store(CreateRoomRequest $request)
    {
        $input = $request->all();

        $room = $this->roomRepository->create($input);

        Flash::success('Room saved successfully.');

        return redirect(route('rooms.index'));
    }

    /**
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $room = $this->roomRepository->find($id);

        if (empty($room)) {
            Flash::error('Room not found');

            return redirect(route('rooms.index'));
        }

        return view('rooms.show')->with('room', $room);
    }

    /**
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $room = $this->roomRepository->find($id);

        if (empty($room)) {
            Flash::error('Room not found');

            return redirect(route('rooms.index'));
        }

        $roomTypes = RoomType::orderBy('name')->get()->pluck('name', 'id');
        $hotels = HotelBranch::orderBy('name')->get()->pluck('name', 'id');
        return view('rooms.edit', ['room' => $room, 'roomTypes' => $roomTypes, 'hotels' => $hotels]);

    }

    /**
     *
     * @param int $id
     * @param UpdateRoomRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoomRequest $request)
    {
        $room = $this->roomRepository->find($id);

        if (empty($room)) {
            Flash::error('Room not found');

            return redirect(route('rooms.index'));
        }

        $room = $this->roomRepository->update($request->all(), $id);

        Flash::success('Room updated successfully.');

        return redirect(route('rooms.index'));
    }

    /**
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $room = $this->roomRepository->find($id);

        if (empty($room)) {
            Flash::error('Room not found');

            return redirect(route('rooms.index'));
        }

        $this->roomRepository->delete($id);

        Flash::success('Room deleted successfully.');

        return redirect(route('rooms.index'));
    }
}
