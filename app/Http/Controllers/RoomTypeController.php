<?php

namespace App\Http\Controllers;

use App\DataTables\RoomTypeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateRoomTypeRequest;
use App\Http\Requests\UpdateRoomTypeRequest;
use App\Repositories\RoomTypeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class RoomTypeController extends AppBaseController
{
    /** @var RoomTypeRepository $roomTypeRepository*/
    private $roomTypeRepository;

    public function __construct(RoomTypeRepository $roomTypeRepo)
    {
        $this->roomTypeRepository = $roomTypeRepo;
    }

    /**
     * Display a listing of the RoomType.
     *
     * @param RoomTypeDataTable $roomTypeDataTable
     *
     * @return Response
     */
    public function index(RoomTypeDataTable $roomTypeDataTable)
    {
        return $roomTypeDataTable->render('room_types.index');
    }

    /**
     * Show the form for creating a new RoomType.
     *
     * @return Response
     */
    public function create()
    {
        return view('room_types.create');
    }

    /**
     * Store a newly created RoomType in storage.
     *
     * @param CreateRoomTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateRoomTypeRequest $request)
    {
        $input = $request->all();

        $roomType = $this->roomTypeRepository->create($input);

        Flash::success('Room Type saved successfully.');

        return redirect(route('roomTypes.index'));
    }

    /**
     * Display the specified RoomType.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $roomType = $this->roomTypeRepository->find($id);

        if (empty($roomType)) {
            Flash::error('Room Type not found');

            return redirect(route('roomTypes.index'));
        }

        return view('room_types.show')->with('roomType', $roomType);
    }

    /**
     * Show the form for editing the specified RoomType.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $roomType = $this->roomTypeRepository->find($id);

        if (empty($roomType)) {
            Flash::error('Room Type not found');

            return redirect(route('roomTypes.index'));
        }

        return view('room_types.edit')->with('roomType', $roomType);
    }

    /**
     * Update the specified RoomType in storage.
     *
     * @param int $id
     * @param UpdateRoomTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoomTypeRequest $request)
    {
        $roomType = $this->roomTypeRepository->find($id);

        if (empty($roomType)) {
            Flash::error('Room Type not found');

            return redirect(route('roomTypes.index'));
        }

        $roomType = $this->roomTypeRepository->update($request->all(), $id);

        Flash::success('Room Type updated successfully.');

        return redirect(route('roomTypes.index'));
    }

    /**
     * Remove the specified RoomType from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $roomType = $this->roomTypeRepository->find($id);

        if (empty($roomType)) {
            Flash::error('Room Type not found');

            return redirect(route('roomTypes.index'));
        }

        $this->roomTypeRepository->delete($id);

        Flash::success('Room Type deleted successfully.');

        return redirect(route('roomTypes.index'));
    }
}
