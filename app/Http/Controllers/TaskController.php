<?php

namespace App\Http\Controllers;

use App\DataTables\TaskDataTable;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\HotelBranch;
use App\Models\Room;
use App\Models\TaskType;
use App\Models\User;
use App\Repositories\TaskRepository;
use Flash;
use Illuminate\Support\Facades\DB;
use Response;

class TaskController extends AppBaseController
{
    /** @var TaskRepository $taskRepository*/
    private $taskRepository;

    public function __construct(TaskRepository $taskRepo)
    {
        $this->taskRepository = $taskRepo;
    }

    /**
     * Display a listing of the Task.
     *
     * @param TaskDataTable $taskDataTable
     *
     * @return Response
     */
    public function index(TaskDataTable $taskDataTable)
    {
        return $taskDataTable->render('tasks.index');
    }

    /**
     * Show the form for creating a new Task.
     *
     * @return Response
     */
    public function create()
    {
        $tasktypes = TaskType::orderBy('name')->get()->pluck('name', 'id');
        $hotels = HotelBranch::orderBy('name')->get()->pluck('name', 'id');
        $rooms = Room::orderBy('name')->get()->pluck('name', 'id');
        $staff = User::where('user_type', '=', 'staff')->get()->pluck('name', 'id');
        $managers = User::where('user_type', '=', 'manager')->get()->pluck('name', 'id');

        return view('tasks.create', [
            'hotels' => $hotels,
            'staff' => $staff,
            'managers' => $managers,
            'rooms' => $rooms,
            'tasktypes' => $tasktypes,
        ]);
    }

    /**
     * Store a newly created Task in storage.
     *
     * @param CreateTaskRequest $request
     *
     * @return Response
     */
    public function store(CreateTaskRequest $request)
    {
        $input = $request->all();

        $task = $this->taskRepository->create($input);

        Flash::success('Task saved successfully.');

        return redirect(route('tasks.index'));
    }

    /**
     * Display the specified Task.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $task = $this->taskRepository->find($id);

        if (empty($task)) {
            Flash::error('Task not found');

            return redirect(route('tasks.index'));
        }

        return view('tasks.show')->with('task', $task);
    }

    /**
     * Show the form for editing the specified Task.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $task = $this->taskRepository->find($id);

        if (empty($task)) {
            Flash::error('Task not found');

            return redirect(route('tasks.index'));
        }

        $tasktypes = TaskType::orderBy('name')->get()->pluck('name', 'id');
        $hotels = HotelBranch::orderBy('name')->get()->pluck('name', 'id');
        $rooms = Room::orderBy('name')->get()->pluck('name', 'id');
        $staff = User::where('user_type', '=', 'staff')->get()->pluck('name', 'id');
        $managers = User::where('user_type', '=', 'manager')->get()->pluck('name', 'id');


        return view('tasks.edit', [
            'hotels' => $hotels,
            'staff' => $staff,
            'rooms' => $rooms,
            'tasktypes' => $tasktypes,
            'task' => $task,
            'managers' => $managers
        ]);

    }

    /**
     * Update the specified Task in storage.
     *
     * @param int $id
     * @param UpdateTaskRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTaskRequest $request)
    {
        $task = $this->taskRepository->find($id);

        if (empty($task)) {
            Flash::error('Task not found');

            return redirect(route('tasks.index'));
        }

        $task = $this->taskRepository->update($request->all(), $id);

        Flash::success('Task updated successfully.');

        return redirect(route('tasks.index'));
    }

    /**
     * Remove the specified Task from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $task = $this->taskRepository->find($id);

        if (empty($task)) {
            Flash::error('Task not found');

            return redirect(route('tasks.index'));
        }

        $this->taskRepository->delete($id);

        Flash::success('Task deleted successfully.');

        return redirect(route('tasks.index'));
    }
}
