<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Booking
 * @package App\Models
 * @version April 15, 2022, 1:19 pm UTC
 *
 * @property integer $user_id
 * @property integer $hotel_id
 * @property integer $room_id
 * @property string $check_in_date
 * @property string $check_out_date
 * @property integer $no_of_guests
 * @property string $special_request
 * @property boolean $is_cancelled
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Booking whereHotel_id($value)
 */
class Booking extends Model
{

    use HasFactory;

    public $table = 'bookings';
    



    public $fillable = [
        'user_id',
        'hotel_id',
        'room_id',
        'check_in_date',
        'check_out_date',
        'no_of_guests',
        'special_request',
        'is_cancelled',
        'image',

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'hotel_id' => 'integer',
        'room_id' => 'integer',
        'image' => 'string',
        'special_request' => 'string',
        'is_cancelled' => 'boolean',
        'line_1',
        'line_2',
        'city',
        'state',
        'zip',
        'country'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'hotel_id' => 'required',
        'room_id' => 'required',
        'check_in_date' => 'required',
        'check_out_date' => 'required',
        'line_1' => 'string',
        'line_2' => 'string',
        'city' => 'string',
        'state' => 'string',
        'zip' => 'string',
        'country' => 'string'
    ];

    public function room()
    {
        return $this->belongsTo(Room::class, 'room_id');
    }

    public function hotel()
    {
        return $this->belongsTo(HotelBranch::class, 'hotel_id');
    }

    public function user()
    {
        return $this->belongsTo(user::class, 'user_id');
    }
}
