<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class HotelBranch
 * @package App\Models
 * @version April 15, 2022, 12:23 pm UTC
 *
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $description
 * @property int $floor_count
 * @property string $star_rate
 * @property boolean $has_parking
 * @property string $address1
 * @property string $address2
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $country
 */
class HotelBranch extends Model
{

    use HasFactory;

    public $table = 'hotel_branches';
    



    public $fillable = [
        'name',
        'email',
        'phone',
        'description',
        'floor_count',
        'star_rate',
        'has_parking',
        'line_1',
        'line_2',
        'city',
        'state',
        'zip',
        'country'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'description' => 'string',
        'star_rate' => 'string',
        'has_parking' => 'boolean',
        'line_1' => 'string',
        'line_2' => 'string',
        'city' => 'string',
        'state' => 'string',
        'zip' => 'string',
        'country' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required'
    ];

    
}
