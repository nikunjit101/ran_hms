<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Payment
 * @package App\Models
 * @version April 15, 2022, 1:28 pm UTC
 *
 * @property integer $booking_id
 * @property number $amount
 * @property string $type
 * @property boolean $is_refund
 */
class Payment extends Model
{

    use HasFactory;

    public $table = 'payments';
    



    public $fillable = [
        'booking_id',
        'amount',
        'type',
        'is_refund'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'booking_id' => 'integer',
        'amount' => 'double',
        'type' => 'string',
        'is_refund' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'booking_id' => 'required',
        'amount' => 'required'
    ];

    public function booking()
    {
        return $this->belongsTo(Booking::class, 'booking_id')->with(['room', 'hotel']);
    }

    
}
