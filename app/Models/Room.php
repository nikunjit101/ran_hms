<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Room
 * @package App\Models
 * @version April 15, 2022, 12:35 pm UTC
 *
 * @property string $name
 * @property integer $room_type
 * @property number $cost
 * @property string $description
 * @property boolean $smoke_friendly
 * @property boolena $pet_friendly
 * @property integer $hotel_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Room whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Room whereHotel_id($value)
 */
class Room extends Model
{

    use HasFactory;

    public $table = 'rooms';

    public $fillable = [
        'name',
        'room_type',
        'cost',
        'smoke_friendly',
        'pet_friendly',
        'hotel_id',
        'image',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'room_type' => 'integer',
        'cost' => 'double',
        'smoke_friendly' => 'boolean',
        'image' => 'string',
        'hotel_id' => 'integer',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'room_type' => 'required'
    ];

    public function roomType()
    {
        return $this->belongsTo(RoomType::class, 'room_type');
    }

    public function hotel()
    {
        return $this->belongsTo(HotelBranch::class, 'hotel_id');
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class , 'room_id');
    }

    
}
