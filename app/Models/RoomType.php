<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class RoomType
 * @package App\Models
 * @version April 15, 2022, 12:27 pm UTC
 *
 * @property string $name
 */
class RoomType extends Model
{

    use HasFactory;

    public $table = 'room_types';
    



    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    
}
