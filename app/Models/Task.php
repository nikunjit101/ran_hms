<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Task
 * @package App\Models
 * @version April 15, 2022, 12:54 pm UTC
 *
 * @property integer $type_id
 * @property integer $room_id
 * @property integer $assigned_to
 * @property integer $assigned_by
 * @property integer $hotel_id
 * @property inetegr $status
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Task whereHotel_id($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Task whereAssigned_to($value)
 */
class Task extends Model
{

    use HasFactory;

    public $table = 'tasks';
    



    public $fillable = [
        'type_id',
        'room_id',
        'assigned_to',
        'assigned_by',
        'hotel_id',
        'status',
        'task_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'type_id' => 'integer',
        'room_id' => 'integer',
        'assigned_to' => 'integer',
        'assigned_by' => 'integer',
        'hotel_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'type_id' => 'required',
        'room_id' => 'required',
        'assigned_to' => 'required',
        'assigned_by' => 'required',
        'hotel_id' => 'required'
    ];

    public function hotel()
    {
        return $this->belongsTo(HotelBranch::class, 'hotel_id');
    }

    public function user()
    {
        return $this->belongsTo(user::class, 'assigned_to');
    }

    public function room()
    {
        return $this->belongsTo(Room::class, 'room_id');
    }

    public function taskType()
    {
        return $this->belongsTo(TaskType::class, 'type_id');
    }
}
