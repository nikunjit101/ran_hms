<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * class user
 * @package app\models
 * @version april 10, 2022, 3:56 pm utc
 *
 * @property string $name
 * @property string $f_name
 * @property string $l_name
 * @property string $email
 * @property string $password
 * @property string $contact_no
 * @property string $id_proof
 * @property string $credit_card
 * @property string $hotel_id
 * @property string $reward_point
 * @method static \Illuminate\Database\Query\Builder|\App\Models\user whereHotel_id($value)
 */
class user extends authenticatable
{

    use hasfactory;

    public $table = 'users';


    public $fillable = [
        'name',
        'f_name',
        'l_name',
        'email',
        'password',
        'contact_no',
        'id_proof',
        'credit_card',
        'reward_point',
        'user_type',
        'line_1',
        'line_2',
        'city',
        'state',
        'zip',
        'country',
        'hotel_id'
    ];

    /**
     * the attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'f_name' => 'string',
        'l_name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'contact_no' => 'string',
        'id_proof' => 'string',
        'credit_card' => 'string',
        'reward_point' => 'string',
        'user_type' => 'string',
        'line_1' => 'string',
        'line_2' => 'string',
        'city' => 'string',
        'state' => 'string',
        'zip' => 'string',
        'country' => 'string',
        'hotel_id' => 'integer'
    ];

    /**
     * validation rules
     *
     * @var array
     */
    public static $rules = [
        'f_name' => 'required',
        'email' => 'required',
        'line_1' => 'required',
        'city' => 'required',
        'state' => 'required',
        'zip' => 'required'

    ];

    public $hidden = ['password'];


}
