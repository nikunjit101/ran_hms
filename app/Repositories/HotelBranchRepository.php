<?php

namespace App\Repositories;

use App\Models\HotelBranch;
use App\Repositories\BaseRepository;

/**
 * Class HotelBranchRepository
 * @package App\Repositories
 * @version April 15, 2022, 12:23 pm UTC
*/

class HotelBranchRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'phone',
        'description',
        'floor_count',
        'star_rate',
        'has_parking',
        'address1',
        'address2',
        'city',
        'state',
        'zip',
        'country'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return HotelBranch::class;
    }
}
