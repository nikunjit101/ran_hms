<?php

namespace App\Repositories;

use App\Models\RoomType;
use App\Repositories\BaseRepository;

/**
 * Class RoomTypeRepository
 * @package App\Repositories
 * @version April 15, 2022, 12:27 pm UTC
*/

class RoomTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RoomType::class;
    }
}
