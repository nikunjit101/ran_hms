<?php

namespace App\Repositories;

use App\Models\TaskType;
use App\Repositories\BaseRepository;

/**
 * Class TaskTypeRepository
 * @package App\Repositories
 * @version April 15, 2022, 12:50 pm UTC
*/

class TaskTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TaskType::class;
    }
}
