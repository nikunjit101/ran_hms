<?php

namespace Database\Factories;

use App\Models\Booking;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Booking::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->randomDigitNotNull,
        'hotel_id' => $this->faker->randomDigitNotNull,
        'room_id' => $this->faker->randomDigitNotNull,
        'check_in_date' => $this->faker->date('Y-m-d H:i:s'),
        'check_out_date' => $this->faker->date('Y-m-d H:i:s'),
        'guests' => $this->faker->word,
        'special_request' => $this->faker->text,
        'is_cancelled' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
