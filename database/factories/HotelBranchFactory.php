<?php

namespace Database\Factories;

use App\Models\HotelBranch;
use Illuminate\Database\Eloquent\Factories\Factory;

class HotelBranchFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = HotelBranch::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
        'email' => $this->faker->word,
        'phone' => $this->faker->word,
        'description' => $this->faker->word,
        'floor_count' => $this->faker->word,
        'star_rate' => $this->faker->word,
        'has_parking' => $this->faker->word,
        'address1' => $this->faker->word,
        'address2' => $this->faker->word,
        'city' => $this->faker->word,
        'state' => $this->faker->word,
        'zip' => $this->faker->word,
        'country' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
