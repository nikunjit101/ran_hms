<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
        'f_name' => $this->faker->word,
        'l_name' => $this->faker->word,
        'email' => $this->faker->word,
        'password' => $this->faker->word,
        'contact_no' => $this->faker->word,
        'id_proof' => $this->faker->word,
        'credit_card' => $this->faker->word,
        'reward_point' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
