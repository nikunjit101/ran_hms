<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->enum('user_type',['admin','customer','staff','manager']);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('f_name', 45)->nullable();
            $table->string('l_name', 45)->nullable();
            $table->string('contact_no', 45)->nullable();
            $table->string('id_proof', 45)->nullable();
            $table->string('credit_card', 45)->nullable();
            $table->string('reward_point', 45)->default(0);
            $table->string('line_1', 45)->nullable();
            $table->string('line_2', 45)->nullable();
            $table->string('city', 30)->nullable();
            $table->string('state', 30)->nullable();
            $table->string('zip', 10)->nullable();
            $table->string('country', 30)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
