<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelBranchesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_branches', function (Blueprint $table) {
            $table->id('id');
            $table->string('name');
            $table->string('email', 45)->nullable();
            $table->string('phone', 15)->nullable();
            $table->text('description')->nullable();
            $table->integer('floor_count')->default(0);
            $table->string('star_rate', 20)->nullable();
            $table->boolean('has_parking')->default(0);
            $table->string('line_1', 45)->nullable();
            $table->string('line_2', 45)->nullable();
            $table->string('city', 30)->nullable();
            $table->string('state', 30)->nullable();
            $table->string('zip', 10)->nullable();
            $table->string('country', 30)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hotel_branches');
    }
}
