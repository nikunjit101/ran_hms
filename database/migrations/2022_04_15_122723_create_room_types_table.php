<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateRoomTypesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('room_types');

        Schema::create('room_types', function (Blueprint $table) {
            $table->id('id');
            $table->string('name', 30);
            $table->timestamps();
        });

        $types = [
            'Single',
            'Double',
            'Triple',
            'Quad',
            'Queen',
            'King',
            'Twin',
        ];

        foreach ($types as $type) {
            DB::table('room_types')->insert([
                'name' => $type,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('room_types');
    }
}
