<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id('id');
            $table->string('name');
            $table->string('image')->nullable();
            $table->unsignedBigInteger('room_type');
            $table->double('cost',11,2)->default(0);
            $table->boolean('smoke_friendly')->default(false);
            $table->boolean('pet_friendly')->default(false);
            $table->unsignedBigInteger('hotel_id');
            $table->text('description')->nullable();
            $table->timestamps();

            $table->foreign('room_type')->references('id')->on('room_types')->onDelete('restrict');
            $table->foreign('hotel_id')->references('id')->on('hotel_branches')->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rooms');
    }
}
