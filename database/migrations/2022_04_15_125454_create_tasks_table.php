<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id('id');
            $table->timestamp('task_date')->nullable();
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('room_id')->nullable();
            $table->unsignedBigInteger('assigned_to');
            $table->unsignedBigInteger('assigned_by');
            $table->unsignedBigInteger('hotel_id')->nullable();
            $table->enum('status', ['Open', 'Close', 'In Progress', 'Pending'])->default('Open');
            $table->timestamps();

            $table->foreign('type_id')->references('id')->on('task_types')->onDelete('restrict');
            $table->foreign('hotel_id')->references('id')->on('hotel_branches')->onDelete('restrict');
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('restrict');
            $table->foreign('assigned_to')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('assigned_by')->references('id')->on('users')->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tasks');
    }
}
