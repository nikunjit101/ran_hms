<?php
use \App\Models\user;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       User::create([
            'f_name' => 'Admin',
            'l_name' => '',
            'name' => 'Admin',
            "email" => "admin@gmail.com",
            "password" => bcrypt('admin'),
            "user_type" => 'admin'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        User::whereEmail('admin@gmail.com')->delete();
    }
}
