@extends('layouts.app')

@section('content')

    <section class="content">
        <div class="card card-solid">

            @include('adminlte-templates::common.errors')

            {!! Form::open(['route' => 'bookings.store']) !!}

            <div class="card-header">
                <h3 class="card-title font-weight-bold">Book Your Stay</h3>
                <div>
                    <a class="btn-sm btn-default float-right"
                       href="{{ route('bookings.index') }}">
                        Back
                    </a>
                </div>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <div class="col-12">
                            <img src="{{ url('images/slider-1.jpeg') }}" class="product-image" alt="Room Image">
                        </div>
                        <div class="col-12 product-image-thumbs">
                            <div class="product-image-thumb active"><img src="{{ url('images/slider-1.jpeg') }}" alt="Room Image"></div>
                            <div class="product-image-thumb" ><img src="{{ url('images/slider-2.jpeg') }}" alt="Room Image"></div>
                            <div class="product-image-thumb" ><img src="{{ url('images/slider-3.jpeg') }}" alt="Room Image"></div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <h3 class="my-1 text-uppercase">{{$room->hotel->name}}</h3>
                        <ul class="ml-0 mb-0 fa-ul text-muted">
                            <li>{{$room->hotel->line_1}}, {{$room->hotel->line_2}}</li>
                            <li>{{$room->hotel->city}}, {{$room->hotel->state}}, {{$room->hotel->zip}}</li>
                            <li>{{$room->hotel->country}}</li>
                            <li>{{$room->hotel->email}}</li>
                            <li>{{$room->hotel->phone}}</span></li>
                        </ul>

                        <hr>
                        <h5>Book On</h5>

                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control float-right" disabled value="{{$check_in_date}} - {{$check_out_date}}">
                            </div>
                        </div>

                        <h5>Room Type</h5>

                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control float-right" disabled value="{{$room->roomType->name}}">
                            </div>
                        </div>

                        <h5>No of Guests</h5>

                        <div class="form-group">
                            <div class="input-group">
                                <input type="number" required onchange="updatePrice(this.value)" class="form-control float-right" max="5" min="1" value="1" name="no_of_guests" id="no_of_guests">
                            </div>
                        </div>


                        <div class="row form-group">
                                <div class="col-6">
                                <h5 class="lead">Payment Methods</h5>
                                <img src="{{ url('images/credit/visa.png') }}" alt="Visa">
                                <img src="{{ url('images/credit/mastercard.png') }}" alt="Mastercard">
                                <img src="{{ url('images/credit/american-express.png') }}" alt="American Express">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" placeholder="xxxx xxxx xxxx xxxx" required class="form-control float-right" oninput="this.value = this.value.replace(/[^0-9. ]/g, '').replace(/(\..*)\./g, '$1');" maxlength="16" value="{{\Illuminate\Support\Facades\Auth::user()->credit_card}}" id="">
                            </div>
                        </div>


                        <h5>Note</h5>

                        <div class="form-group">
                            <div class="input-group">
                                {!! Form::textarea('special_request', null, ['class' => 'form-control','rows'=>3]) !!}
                            </div>
                        </div>

                        <div class="bg-light py-2 px-3 mt-4">
                            <div class="listingPrice_numbers"><span id="discountPrice" class="listingPrice_lastPrice">${{round($room->cost - \Illuminate\Support\Facades\Auth::user()->reward_point, 2)}}</span><span class="listingPrice_slashed_lastPrice d-body-lg" id="totalPrice">${{$room->cost}}</span><span class="listing_lastPrice_percentage">{{round((($room->cost - ($room->cost - \Illuminate\Support\Facades\Auth::user()->reward_point))/$room->cost)*100)}}% off</span></div>
                            <div class="col-md-12">
                                <span class="listingPrice_perRoomNight">inclusive of all taxes</span>
                            </div>
                        </div>

                        <div class="mt-4">
                            {!! Form::submit('Confirm', ['class' => 'btn btn-block btn-warning btn-flat cnfBtn']) !!}
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <nav class="w-100">
                        <div class="nav nav-tabs" id="product-tab" role="tablist">
                            <a class="nav-item nav-link active" id="hotel-desc-tab" data-toggle="tab" href="#hotel-desc" role="tab" aria-controls="hotel-desc" aria-selected="true">Description</a>
                            <a class="nav-item nav-link" id="hotel-amenity-tab" data-toggle="tab" href="#hotel-amenity" role="tab" aria-controls="hotel-amenity" aria-selected="false">Amenities</a>
                        </div>
                    </nav>
                    <div class="tab-content p-3" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="hotel-desc" role="tabpanel" aria-labelledby="hotel-desc-tab">
                            {{$room->description}}</div>
                        <div class="tab-pane fade" id="hotel-amenity" role="tabpanel" aria-labelledby="hotel-amenity-tab">
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="form-control amenity1">
                                            <i class="fas fa-wifi"></i>
                                            Free Internet Access</div>
                                    </div>
                                    @if($room->pet_friendly == 1)
                                        <div class="form-group">
                                            <div class="form-control amenity1">
                                                <i class="fas fa-dog"></i>
                                                Pet Friendly</div>
                                        </div>
                                    @endif

                                    @if($room->smoke_friendly == 1)
                                        <div class="form-group">
                                            <div class="form-control amenity1">
                                                <i class="fas fa-smoking"></i>
                                                Smoke Friendly</div>
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <div class="form-control amenity1">
                                            <i class="fas fa-parking"></i>
                                            Parking</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            {{ Form::hidden('user_id', \Illuminate\Support\Facades\Auth::user()->id) }}
            {{ Form::hidden('room_id', $room->id) }}
            {{ Form::hidden('hotel_id', $room->hotel->id) }}
            {{ Form::hidden('check_in_date', $check_in_date) }}
            {{ Form::hidden('check_out_date', $check_out_date) }}
            {{ Form::hidden('amount', $room->cost - \Illuminate\Support\Facades\Auth::user()->reward_point, array('id' => 'amount')) }}

            {!! Form::close() !!}

        </div>

    </section>





    <script>

        function updatePrice(value){
            discountPrice = {{$room->cost - \Illuminate\Support\Facades\Auth::user()->reward_point}}*value;
            $('#discountPrice').text('$'+ parseFloat(discountPrice).toFixed(2));
            $('#totalPrice').text('$'+ {{$room->cost}}*value);
            $('#amount').val('{{$room->cost - \Illuminate\Support\Facades\Auth::user()->reward_point}}'* value);
        }

    </script>

@endsection