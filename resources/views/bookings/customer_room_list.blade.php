@extends('layouts.app')

@section('content')

    @include('flash::message')

    <section class="content-header">
        <div class="container-fluid card card-solid">
            <div class="card-header">
                <h3 class="card-title font-weight-bold">Welcome! Book your stay</h3>
            </div>
            <div class="row card-body pb-1">
                <div class="form-group">
                    <div class="input-group row">
                        {!! Form::select('country', $countries, $location ? $location : 'US', ['class' => 'form-control','id' => 'location']) !!}
                        <input type="text" class="form-control float-right" id="reservation">
                        <div class="input-group-append input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <a id="url" href="{{ route('availablerooms',['check_in_date'=>$check_in_date, 'check_out_date'=>$check_out_date, 'location'=>$location] )}}" class="btn btn-block btn-warning btn-flat" style="color: #ffffff; background-color: #fd7e14; border-color: #fd7e14">Search</a>
                    {{ Form::hidden('check_in_date', $check_in_date , ['id'=>'check_in_date']) }}
                    {{ Form::hidden('check_out_date', $check_out_date , ['id'=>'check_out_date']) }}
                </div>

            </div>
        </div>
    </section>

    <section class="content">
        <div class="card card-solid">
            <div class="card-body pb-0">
                <div class="row d-flex align-items-stretch">
                    @foreach($rooms as $room)
                        @if($room->is_available == true)
                            <div class="col-12 col-sm-6 col-md-6 align-items-stretch">
                            <div class="card bg-light">
                                <div class="card-header text-muted border-bottom-0">
                                    <h6 class="text-uppercase">{{$room->name}} : {{$room->roomType->name}}</h6>
                                    <span class="badge bg-success">{{$room->hotel->star_rate}} <i class="fas fa-star"></i></span>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="row">
                                        <div class="col-7">
                                            <h2 class="lead text-uppercase"><b>{{$room->hotel->name}}</b></h2>

                                            <ul class="ml-0 mb-0 fa-ul text-muted">
                                                {{--                            <li class="small"><span class="fa-li"><i class="fas fa-map-marker-alt"></i></span> Address: {{$room->hotel->line_1}}, {{$room->hotel->line_2}}, {{$room->hotel->city}} {{$room->hotel->zip}}, {{$room->hotel->state}}</li>--}}
                                                <li>{{$room->hotel->line_1}}, {{$room->hotel->line_2}}</li>
                                                <li>{{$room->hotel->city}}, {{$room->hotel->state}}, {{$room->hotel->zip}}</li>
                                                <li>{{$room->hotel->country}}</li>
                                                <li>{{$room->hotel->email}}</li>
                                                <li>{{$room->hotel->phone}}</span></li>
                                            </ul>
                                        </div>
                                        <div class="col-5 text-center">
                                            <img src="{{ url('images/slider-1.jpeg') }}" alt="user-avatar" style="border-radius: 10px" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="col-m-12">
                                        <div class="listingPrice_numbers"><span class="listingPrice_finalPrice">${{round($room->cost - \Illuminate\Support\Facades\Auth::user()->reward_point, 2)}}</span><span class="listingPrice_slashedPrice d-body-lg">${{$room->cost}}</span><span class="listingPrice_percentage">{{round((($room->cost - ($room->cost - \Illuminate\Support\Facades\Auth::user()->reward_point))/$room->cost)*100)}}% off</span></div>
                                        <div class="col-md-12">
                                            <span class="listingPrice_perRoomNight">per room per night</span>
                                        </div>

                                        <div class="text-reset text-right">
                                            <a href="{{ route('availablebookroom',['id'=>$room->id,'check_in_date'=>$check_in_date, 'check_out_date'=>$check_out_date])}}" class="btn btn-sm bg-teal">
                                                <i class="nav-icon fas fa-shopping-cart"></i> Book Now
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>

    </section>

    <script>
        function call(){
            alert('HI');
        }


        $('#reservation').daterangepicker({
            opens: 'left',
            startDate: Date.parse('{{$check_in_date ? date($check_in_date) : null}}'),
            endDate: Date.parse('{{$check_out_date ? date($check_out_date) : null}}'),
        }, function(start, end) {
            var url = "{{url('/availablerooms')}}"+"/"+start.format('YYYY-MM-DD hh:mm:ss')+"/"+end.format('YYYY-MM-DD hh:mm:ss')+"/"+$('#location option:selected').val();
            console.log(url);
            $('#url').attr("href", url);
        });
    </script>
@endsection
