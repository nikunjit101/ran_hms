<!-- Check In Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('check_in_date', 'Check In Date') !!}
    {!! Form::text('check_in_date', null, ['class' => 'form-control','id'=>'check_in_date']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#check_in_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush


<div class="form-group col-sm-6">
    {!! Form::label('check_out_date', 'Check Out Date') !!}
    {!! Form::text('check_out_date', null, ['class' => 'form-control','id'=>'check_out_date']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#check_out_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush


<div class="form-group col-sm-6">
    {!! Form::label('hotel_id', 'Hotel') !!}
    {!! Form::select('hotel_id',$hotels,null,['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('room_id', 'Room') !!}
    {!! Form::select('room_id',$rooms,null,['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'Customer') !!}
    {!! Form::select('user_id',$users,null,['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('no_of_guests', 'No of Guests') !!}
    {!! Form::number('no_of_guests', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('special_request', 'Special Request') !!}
    {!! Form::text('special_request', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('is_cancelled', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('is_cancelled', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('is_cancelled', 'Is Cancelled', ['class' => 'form-check-label']) !!}
    </div>
</div>
