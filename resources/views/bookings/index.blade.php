@extends('layouts.app')

@section('content')
    <div class="content">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Bookings</h3>
                <a class="btn-sm btn-primary float-right"
                   href="{{ route('bookings.create') }}">
                    <i class="fas fa-plus"></i>
                </a>
            </div>
            <div class="card-body">
                @include('bookings.table')
            </div>

        </div>
    </div>
@endsection

