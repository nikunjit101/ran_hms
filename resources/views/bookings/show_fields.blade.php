<div class="col-sm-12">
    {!! Form::label('user_id', 'User') !!}
    <p>{{ $booking->user_id }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('hotel_id', 'Hotel') !!}
    <p>{{ $booking->hotel_id }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('room_id', 'Room') !!}
    <p>{{ $booking->room_id }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('check_in_date', 'Check In Date') !!}
    <p>{{ $booking->check_in_date }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('check_out_date', 'Check Out Date') !!}
    <p>{{ $booking->check_out_date }}</p>
</div>


<div class="col-sm-12">
    {!! Form::label('guests', 'No of Guests') !!}
    <p>{{ $booking->no_of_guests }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('special_request', 'Special Request') !!}
    <p>{{ $booking->special_request }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('is_cancelled', 'Is Cancelled') !!}
    <p>{{ $booking->is_cancelled }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At') !!}
    <p>{{ $booking->created_at }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At') !!}
    <p>{{ $booking->updated_at }}</p>
</div>

