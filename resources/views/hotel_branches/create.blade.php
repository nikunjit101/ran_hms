@extends('layouts.app')

@section('content')

    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Create Hotel</h3>
        </div>

        @include('adminlte-templates::common.errors')

        {!! Form::open(['route' => 'hotelBranches.store']) !!}

        <div class="card-body">

            <div class="row">
                @include('hotel_branches.fields')
            </div>

        </div>

        <div class="card-footer">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            <a href="{{ route('hotelBranches.index') }}" class="btn btn-default">Cancel</a>
        </div>

        {!! Form::close() !!}
    </div>
@endsection
