{!! Form::open(['route' => ['hotelBranches.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('hotelBranches.show', $id) }}" class='btn btn-default'>
        <i class="fa fa-eye"></i>
    </a>
    <a href="{{ route('hotelBranches.edit', $id) }}" class='btn btn-default'>
        <i class="fa fa-edit"></i>
    </a>
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger',
        'onclick' => "return confirm('Are you sure?')"
    ]) !!}
</div>
{!! Form::close() !!}
