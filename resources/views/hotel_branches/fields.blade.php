<div class="form-group col-sm-6">
    {!! Form::label('name', 'Hotel Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('floor_count', 'Floor Count') !!}
    {!! Form::number('floor_count', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('star_rate', 'Star Rate') !!}
    {{ Form::select('star_rate', [1=>1,2=>2,3=>3,4=>4,5=>5], null, ['class' => 'form-control','id' => 'star_rate']) }}
</div>

<div class="form-group col-sm-12">
    <div class="form-check">
        {!! Form::hidden('has_parking', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('has_parking', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('has_parking', 'Has Parking', ['class' => 'form-check-label']) !!}
    </div>
</div>

@if(!isset($hotel_branch))
    <div class="form-group col-sm-6">
        {!! Form::label('line_1', 'Line 1') !!}
        {!! Form::text('line_1', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('line_2', 'Line 2') !!}
        {!! Form::text('line_2', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('city', 'City') !!}
        {!! Form::text('city', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('state', 'State') !!}
        {!! Form::text('state', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('zip', 'Zip') !!}
        {!! Form::text('zip', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('country', 'Country') !!}
        {!! Form::select('country', $countries,null,['class' => 'form-control']) !!}
    </div>
@else
    <div class="form-group col-sm-6">
        {!! Form::label('line_1', 'Line 1') !!}
        {!! Form::text('line_1', ($hotel_branch->line_1)?$hotel_branch->line_1:null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('line_2', 'Line 2') !!}
        {!! Form::text('line_2', ($hotel_branch->line_2)?$hotel_branch->line_2:null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('city', 'City') !!}
        {!! Form::text('city', ($hotel_branch->city)?$hotel_branch->city:null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('state', 'State') !!}
        {!! Form::text('state', ($hotel_branch->state)?$hotel_branch->state:null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('zip', 'Zip') !!}
        {!! Form::text('zip', ($hotel_branch->zip)?$hotel_branch->zip:null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('country', 'Country') !!}
        {!! Form::select('country', $countries,($hotel_branch->country)?$hotel_branch->country:null,['class' => 'form-control']) !!}
    </div>
@endif
