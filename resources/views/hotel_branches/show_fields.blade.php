<div class="col-sm-12">
    {!! Form::label('name', 'Name') !!}
    <p>{{ $hotelBranch->name }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('email', 'Email') !!}
    <p>{{ $hotelBranch->email }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('phone', 'Phone') !!}
    <p>{{ $hotelBranch->phone }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('description', 'Description') !!}
    <p>{{ $hotelBranch->description }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('floor_count', 'Floor Count') !!}
    <p>{{ $hotelBranch->floor_count }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('star_rate', 'Star Rate') !!}
    <p>{{ $hotelBranch->star_rate }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('has_parking', 'Has Parking') !!}
    <p>{{ $hotelBranch->has_parking }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('address1', 'Address1') !!}
    <p>{{ $hotelBranch->line_1 }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('address2', 'Address2') !!}
    <p>{{ $hotelBranch->line_2 }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('city', 'City') !!}
    <p>{{ $hotelBranch->city }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('state', 'State') !!}
    <p>{{ $hotelBranch->state }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('zip', 'Zip') !!}
    <p>{{ $hotelBranch->zip }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('country', 'Country') !!}
    <p>{{ $hotelBranch->country }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At') !!}
    <p>{{ $hotelBranch->created_at }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At') !!}
    <p>{{ $hotelBranch->updated_at }}</p>
</div>

