@if(\Illuminate\Support\Facades\Auth::user()->user_type == 'admin' ||
 \Illuminate\Support\Facades\Auth::user()->user_type == 'manager' ||
 \Illuminate\Support\Facades\Auth::user()->user_type == 'customer')
    <li class="nav-item">
        <a href="{{ route('bookings.index') }}"
           class="nav-link {{ Request::is('bookings*') ? 'active' : '' }}">
            <i class="nav-icon fas fa-shopping-cart"></i>
            <p>Bookings</p>
        </a>
    </li>
@endif

@if(\Illuminate\Support\Facades\Auth::user()->user_type == 'admin' || \Illuminate\Support\Facades\Auth::user()->user_type == 'manager')
<li class="nav-item">
    <a href="{{ route('users.index') }}"
       class="nav-link {{ Request::is('users*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-user"></i>
        <p>{{\Illuminate\Support\Facades\Auth::user()->user_type == 'admin'? 'Users' : 'Staff'}}</p>
    </a>
</li>
@endif

@if(\Illuminate\Support\Facades\Auth::user()->user_type == 'admin')
<li class="nav-item">
    <a href="{{ route('hotelBranches.index') }}"
       class="nav-link {{ Request::is('hotelBranches*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-hotel"></i>
        <p>Hotels</p>
    </a>
</li>
@endif

@if(\Illuminate\Support\Facades\Auth::user()->user_type == 'admin' ||
\Illuminate\Support\Facades\Auth::user()->user_type == 'manager')
<li class="nav-item">
    <a href="{{ route('rooms.index') }}"
       class="nav-link {{ Request::is('rooms*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-bed"></i>
        <p>Rooms</p>
    </a>
</li>
@endif

@if(\Illuminate\Support\Facades\Auth::user()->user_type == 'admin' ||
\Illuminate\Support\Facades\Auth::user()->user_type == 'manager' ||
\Illuminate\Support\Facades\Auth::user()->user_type == 'staff')
<li class="nav-item">
    <a href="{{ route('tasks.index') }}"
       class="nav-link {{ Request::is('tasks*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-tasks"></i>
        <p>Tasks</p>
    </a>
</li>
@endif

@if(\Illuminate\Support\Facades\Auth::user()->user_type == 'admin')
<li class="nav-item">
    <a href="{{ route('payments.index') }}"
       class="nav-link {{ Request::is('payments*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-file"></i>
        <p>Report</p>
    </a>
</li>
@endif

@if(\Illuminate\Support\Facades\Auth::user()->user_type == 'admin')
<li class="nav-item {{ Request::is('taskTypes*') || Request::is('roomTypes*') ? 'menu-is-opening menu-open' : '' }}">
    <a href="#" class="nav-link {{ Request::is('taskTypes*') || Request::is('roomTypes*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-cog"></i>
        <p>
            Settings
            <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview" style="display: {{ Request::is('taskTypes*') || Request::is('roomTypes*') ? 'block' : 'none' }};">
        <li class="nav-item">
            <a href="{{ route('taskTypes.index') }}"
               class="nav-link {{ Request::is('taskTypes*') ? 'active' : '' }}">
                <i class="nav-icon fas fa-bars"></i>
                <p>Task Types</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('roomTypes.index') }}"
               class="nav-link {{ Request::is('roomTypes*') ? 'active' : '' }}">
                <i class="nav-icon fas fa-bed"></i>
                <p>Room Types</p>
            </a>
        </li>
    </ul>
</li>
@endif


