@extends('layouts.app')

@section('content')
    <div class="content">

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Payments</h3>
            </div>
            <div class="card-body">
                <div class="tab-pane active" id="bookingHistory">
                    <div class="timeline timeline-inverse">
                        @foreach($payments as $payment)
                            <div class="time-label">
                            <span class="bg-danger">
                              {{$payment->created_at}}
                            </span>
                            </div>
                            <div>
                                <i class="nav-icon fas bg-primary fa-shopping-cart"></i>

                                <div class="timeline-item col-md-6 border-0">
                                    <div class="align-items-stretch">
                                        <div class="card bg-light">
                                            <div class="card-header text-muted border-bottom-0">
                                                <h6 class="text-uppercase">{{$payment->booking->room->name}}
                                                    : {{$payment->booking->room->roomType->name}}</h6>
                                                <span class="badge bg-success">{{$payment->booking->hotel->star_rate}} <i
                                                            class="fas fa-star"></i></span>
                                            </div>
                                            <div class="card-body pt-0">
                                                <div class="row">
                                                    <div class="col-7">
                                                        <h2 class="lead text-uppercase">
                                                            <b>{{$payment->booking->room->hotel->name}}</b></h2>

                                                        <p class="text-muted text-sm"><b>Description: </b>
                                                            {{$payment->booking->room->hotel->description}} </p>
                                                        <ul class="ml-4 mb-0 fa-ul text-muted">
                                                            <li class="small"><span class="fa-li"><i
                                                                            class="fas fa-map-marker-alt"></i></span>
                                                                Address:
                                                                {{$payment->booking->room->hotel->line_1}},
                                                                {{$payment->booking->room->hotel->line_2}},
                                                                {{$payment->booking->room->hotel->city}}
                                                                {{$payment->booking->room->hotel->zip}},
                                                                {{$payment->booking->room->hotel->state}}</li>
                                                            <li class="small"><span class="fa-li"><i
                                                                            class="fas fa-mobile"></i></span> Phone No:
                                                                {{$payment->booking->room->hotel->phone}}</li>
                                                            <li class="small"><span class="fa-li"><i
                                                                            class="fas fa-at"></i></span> Email ID:
                                                                {{$payment->booking->room->hotel->email}}</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                <div class="col-m-12">
                                                    <div class="listingPrice_numbers"><span
                                                                class="listingPrice_finalPrice">$ {{$payment->amount}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div>
                            <i class="far fa-clock bg-gray"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
