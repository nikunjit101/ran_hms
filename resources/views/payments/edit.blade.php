@extends('layouts.app')

@section('content')

    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit Payment</h3>
        </div>

        @include('adminlte-templates::common.errors')

        {!! Form::model($payment, ['route' => ['payments.update', $payment->id], 'method' => 'patch']) !!}

        <div class="card-body">
            <div class="row">
                @include('payments.fields')
            </div>
        </div>

        <div class="card-footer">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            <a href="{{ route('payments.index') }}" class="btn btn-default">Cancel</a>
        </div>

        {!! Form::close() !!}
    </div>
@endsection
