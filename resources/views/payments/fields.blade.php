<div class="form-group col-sm-6">
    {!! Form::label('booking_id', 'Booking:') !!}
    {!! Form::select('booking_id',$bookings,null,['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('type', 'Status') !!}
    {!! Form::select('type',['0','1'],null,['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('amount', 'Amount') !!}
    {!! Form::number('amount',null,['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('is_refund', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('is_refund', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('is_refund', 'Is Refund', ['class' => 'form-check-label']) !!}
    </div>
</div>
