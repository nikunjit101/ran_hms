@extends('layouts.app')

@section('content')

<div class="content">

    <div class="clearfix"></div>

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Hotel Booking Report</h3>
        </div>
        <div class="card-body">
            <div class="input-group row col-9">
                {!! Form::select('hotel_id',$hotels,null,['class' => 'form-control','id'=>'hotel']) !!}
                <input name="date" type="text" class="form-control float-right" id="date">
                <div class="input-group-append input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                </div>
                <input type="submit" class="btn btn-primary" name="Generate" id="generate"/>
            </div>

            <div class="container pt-4" style="padding-left: 0;padding-right: 0">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Check In Date</th>
                            <th>Room</th>
                            <th>Customer</th>
                            <th>Cost</th>
                        </tr>
                        </thead>
                        <tbody id="tbody">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    var startDate = Date.now();
    var endDate = Date.now();
    $('#date').daterangepicker({
        opens: 'left'
    }, function(start, end) {
        startDate = start.format('YYYY-MM-DD hh:mm:ss');
        endDate = end.format('YYYY-MM-DD hh:mm:ss');
    });


    $('#generate').click(function(){
        $.ajax({
            url: "/generatereport/"+ startDate +'/' + endDate +'/' +$('#hotel option:selected').val(),
            type: "GET",
            success:function(response){
                console.log(response.bookings);
                for(i=0;i<response.bookings.length;i++){
                    $('#tbody').append('<tr><td>'+response.bookings[i].check_in_date+'</td><td>'+response.bookings[i].room.name+'</td><td>'+response.bookings[i].user.name+'</td><td>'+response.bookings[i].room.cost+'</td></tr>')
                }

            },
            error: function(error) {
                console.log(error);
            }
        })
    });
</script>
@endsection

