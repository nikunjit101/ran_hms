<div class="col-sm-12">
    {!! Form::label('booking_id', 'Booking Id:') !!}
    <p>{{ $payment->booking_id }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{{ $payment->amount }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('type', 'Type:') !!}
    <p>{{ $payment->type }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('is_refund', 'Is Refund:') !!}
    <p>{{ $payment->is_refund }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $payment->created_at }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $payment->updated_at }}</p>
</div>

