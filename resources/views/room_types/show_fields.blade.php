<div class="col-sm-12">
    {!! Form::label('name', 'Name') !!}
    <p>{{ $roomType->name }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At') !!}
    <p>{{ $roomType->created_at }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At') !!}
    <p>{{ $roomType->updated_at }}</p>
</div>

