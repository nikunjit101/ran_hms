@extends('layouts.app')

@section('content')

    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit Room</h3>
        </div>

        @include('adminlte-templates::common.errors')

        {!! Form::model($room, ['route' => ['rooms.update', $room->id], 'method' => 'patch']) !!}

        <div class="card-body">
            <div class="row">
                @include('rooms.fields')
            </div>
        </div>

        <div class="card-footer">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            <a href="{{ route('rooms.index') }}" class="btn btn-default">Cancel</a>
        </div>

        {!! Form::close() !!}
    </div>
@endsection
