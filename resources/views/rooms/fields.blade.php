{{--<script>--}}
{{--    function readURL(input) {--}}
{{--        if (input.files && input.files[0]) {--}}
{{--            var reader = new FileReader();--}}

{{--            reader.onload = function (e) {--}}
{{--                $('#preview')--}}
{{--                    .attr('src', e.target.result)--}}
{{--                    .width(100)--}}
{{--                    .height(100);--}}
{{--            };--}}

{{--            reader.readAsDataURL(input.files[0]);--}}
{{--        }--}}
{{--    }--}}
{{--</script>--}}

{{--@if(isset($room))--}}
{{--    <?php--}}
{{--    $img = $room->image;--}}
{{--    $passwordValidation = '';--}}
{{--    ?>--}}
{{--@else--}}
{{--    <?php--}}
{{--    $img = '';--}}
{{--    $passwordValidation = 'required';--}}
{{--    ?>--}}
{{--@endif--}}


<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('cost', 'Cost:') !!}
    {!! Form::number('cost', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('smoke_friendly', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('smoke_friendly', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('smoke_friendly', 'Smoke Friendly', ['class' => 'form-check-label']) !!}
    </div>
</div>


<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('pet_friendly', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('pet_friendly', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('pet_friendly', 'Pet Friendly', ['class' => 'form-check-label']) !!}
    </div>
</div>

<div class="form-group col-sm-6">
    {!! Form::label('room_type', 'Room Type') !!}
    {!! Form::select('room_type',$roomTypes,null,['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-6">
    {!! Form::label('hotel_id', 'Hotel') !!}
    {!! Form::select('hotel_id',$hotels,null,['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

{{--<div class="col-md-6">--}}
{{--    <!-- Image Field -->--}}
{{--    <div class="col-sm-12 form-group text-left">--}}
{{--        <img id="preview" class="profile-user-img img-responsive img-circle pull-left"--}}
{{--             src="{{asset('images/'.$img)}}"--}}
{{--             onerror="this.src='{{asset('images/user.png')}}'"--}}
{{--             alt="User profile picture">--}}

{{--    </div>--}}
{{--    <div style="padding-bottom: 15px">--}}
{{--        {!! Form::file('image', ['class' => '','onchange'=>'readURL(this);']) !!}--}}
{{--    </div>--}}
{{--</div>--}}



