<div class="col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $room->name }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('room_type', 'Room Type:') !!}
    <p>{{ $room->room_type }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('cost', 'Cost:') !!}
    <p>{{ $room->cost }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('smoke_friendly', 'Smoke Friendly:') !!}
    <p>{{ $room->smoke_friendly }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('pet_friendly', 'Pet Friendly:') !!}
    <p>{{ $room->pet_friendly }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('hotel_id', 'Hotel Id:') !!}
    <p>{{ $room->hotel_id }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $room->created_at }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $room->updated_at }}</p>
</div>

