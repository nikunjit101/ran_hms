@extends('layouts.app')

@section('content')

    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit Task Type</h3>
        </div>

        @include('adminlte-templates::common.errors')

        {!! Form::model($taskType, ['route' => ['taskTypes.update', $taskType->id], 'method' => 'patch']) !!}

        <div class="card-body">
            <div class="row">
                @include('task_types.fields')
            </div>
        </div>

        <div class="card-footer">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            <a href="{{ route('taskTypes.index') }}" class="btn btn-default">Cancel</a>
        </div>

        {!! Form::close() !!}
    </div>
@endsection
