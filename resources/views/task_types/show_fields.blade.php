<div class="col-sm-12">
    {!! Form::label('name', 'Name') !!}
    <p>{{ $taskType->name }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At') !!}
    <p>{{ $taskType->created_at }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At') !!}
    <p>{{ $taskType->updated_at }}</p>
</div>

