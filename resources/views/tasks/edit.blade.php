@extends('layouts.app')

@section('content')

    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit Task</h3>
        </div>

        @include('adminlte-templates::common.errors')

        {!! Form::model($task, ['route' => ['tasks.update', $task->id], 'method' => 'patch']) !!}

        <div class="card-body">
            <div class="row">
                @include('tasks.fields')
            </div>
        </div>

        <div class="card-footer">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            <a href="{{ route('tasks.index') }}" class="btn btn-default">Cancel</a>
        </div>

        {!! Form::close() !!}
    </div>
@endsection
