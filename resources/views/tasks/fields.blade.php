<div class="form-group col-sm-6">
    {!! Form::label('type_id', 'Task Type') !!}
    {!! Form::select('type_id',$tasktypes,null,['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('task_date', 'Date') !!}
    {!! Form::date('task_date',null,['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-6">
    {!! Form::label('hotel_id', 'Hotel') !!}
    {!! Form::select('hotel_id',$hotels,null,['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('room_id', 'Room') !!}
    {!! Form::select('room_id',$rooms,null,['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('assigned_to', 'Assigned To') !!}
    {!! Form::select('assigned_to',$staff,null,['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('assigned_by', 'Assigned By') !!}
    {!! Form::select('assigned_by',$managers,null,['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status') !!}
    {!! Form::select('status',['Open', 'Close', 'In Progress', 'Pending'],null,['class' => 'form-control']) !!}
</div>
