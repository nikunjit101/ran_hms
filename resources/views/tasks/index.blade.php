@extends('layouts.app')

@section('content')

<div class="content">

    @include('flash::message')

    <div class="clearfix"></div>

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Tasks</h3>
            <a class="btn-sm btn-primary float-right"
               href="{{ route('tasks.create') }}">
                <i class="fas fa-plus"></i>
            </a>
        </div>
        <div class="card-body">
            @include('tasks.table')
        </div>

    </div>
</div>

{{--    <div class="content px-3">--}}

{{--        @include('flash::message')--}}

{{--        <div class="clearfix"></div>--}}

{{--        <div class="card">--}}
{{--            <div class="card-body p-0">--}}
{{--                @include('tasks.table')--}}

{{--                <div class="card-footer clearfix">--}}
{{--                    <div class="float-right">--}}
{{--                        --}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--        </div>--}}
{{--    </div>--}}

@endsection

