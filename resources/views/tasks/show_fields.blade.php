<div class="col-sm-12">
    {!! Form::label('type_id', 'Type Id:') !!}
    <p>{{ $task->type_id }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('room_id', 'Room Id:') !!}
    <p>{{ $task->room_id }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('assigned_to', 'Assigned To:') !!}
    <p>{{ $task->assigned_to }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('assigned_by', 'Assigned By:') !!}
    <p>{{ $task->assigned_by }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('hotel_id', 'Hotel Id:') !!}
    <p>{{ $task->hotel_id }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $task->status }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $task->created_at }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $task->updated_at }}</p>
</div>

