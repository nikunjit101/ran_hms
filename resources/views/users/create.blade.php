@extends('layouts.app')

@section('content')


    <div class="card card-primary">
        <div class="card-header">

            @if(\Illuminate\Support\Facades\Auth::user()->user_type == 'manager')
                <h3 class="card-title">Create Staff</h3>
            @else
                <h3 class="card-title">Create User</h3>
            @endif
        </div>

        @include('adminlte-templates::common.errors')

        {!! Form::open(['route' => 'users.store']) !!}

        <div class="card-body">

            <div class="row">
                @include('users.fields')
            </div>

        </div>

        <div class="card-footer">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            <a href="{{ route('users.index') }}" class="btn btn-default">Cancel</a>
        </div>

        {!! Form::close() !!}
    </div>
@endsection
