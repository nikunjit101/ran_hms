@extends('layouts.app')

@section('content')

    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit Profile</h3>
        </div>

        @include('adminlte-templates::common.errors')

        {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch']) !!}

        <div class="card-body">
            <div class="row">
                @include('users.fields')
            </div>
        </div>

        <div class="card-footer">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}

            <a href="{{ route(\Illuminate\Support\Facades\Auth::user()->user_type == 'staff'?'tasks.index':'users.index') }}" class="btn btn-default">Cancel</a>
        </div>

        {!! Form::close() !!}
    </div>


@endsection
