<div class="form-group col-sm-6">
    {!! Form::label('f_name', 'First Name') !!}
    {!! Form::text('f_name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('l_name', 'Last Name') !!}
    {!! Form::text('l_name', null, ['class' => 'form-control']) !!}
</div>

@if(!isset($user))
    <div class="form-group col-sm-6">
        {!! Form::label('email', 'Email') !!}
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
    </div>
@else
    {{ Form::hidden('email', $user->email) }}
@endif

<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('contact_no', 'Contact No') !!}
    {!! Form::text('contact_no', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('id_proof', 'Id Proof') !!}
    {!! Form::text('id_proof', null, ['class' => 'form-control']) !!}
</div>

<!-- Credit Card Field -->
{{--<div class="form-group col-sm-6">--}}
{{--    {!! Form::label('credit_card', 'Credit Card') !!}--}}
{{--    {!! Form::text('credit_card', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}


@if(\Illuminate\Support\Facades\Auth::user()->user_type == 'manager')
    {{ Form::hidden('hotel_id', \Illuminate\Support\Facades\Auth::user()->hotel_id) }}
    {{ Form::hidden('user_type', 'staff') }}
@endif


@if(!isset($user))
    <div class="form-group col-sm-6">
        {!! Form::label('line_1', 'Line 1') !!}
        {!! Form::text('line_1', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('line_2', 'Line 2') !!}
        {!! Form::text('line_2', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('city', 'City') !!}
        {!! Form::text('city', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('state', 'State') !!}
        {!! Form::text('state', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('zip', 'Zip') !!}
        {!! Form::text('zip', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('country', 'Country') !!}
        {!! Form::select('country', $countries,null,['class' => 'form-control']) !!}
    </div>

    @if(\Illuminate\Support\Facades\Auth::user()->user_type == 'admin')
        <!-- Reward Point Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('user_type', 'Role') !!}
            {!! Form::select('user_type',[
                    'manager' => "Manager",
                    'staff' => "Staff"]
                    ,null,['class' => 'form-control', 'onchange' => 'onChangeRole()']) !!}
        </div>

        <div class="form-group col-sm-6" id="hotel_grp">
            {!! Form::label('hotel_id', 'Hotel') !!}
            {!! Form::select('hotel_id',$hotels,null,['class' => 'form-control']) !!}
        </div>
    @endif

@else
    <div class="form-group col-sm-6">
        {!! Form::label('line_1', 'Line 1') !!}
        {!! Form::text('line_1', ($user->line_1)?$user->line_1:null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('line_2', 'Line 2') !!}
        {!! Form::text('line_2', ($user->line_2)?$user->line_2:null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('city', 'City') !!}
        {!! Form::text('city', ($user->city)?$user->city:null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('state', 'State') !!}
        {!! Form::text('state', ($user->state)?$user->state:null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('zip', 'Zip') !!}
        {!! Form::text('zip', ($user->zip)?$user->zip:null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('country', 'Country') !!}
        {!! Form::select('country', $countries,($user->country)?$user->country:null,['class' => 'form-control']) !!}
    </div>

    @if($user->user_type == 'customer')
        <div class="form-group col-sm-6">
            {!! Form::label('reward_point', 'Reward Point') !!}
            {!! Form::text('reward_point', ($user->reward_point)?$user->reward_point:0, ['class' => 'form-control']) !!}
        </div>
    @elseif($user->user_type == 'admin')
        <div class="form-group col-sm-6">
            {!! Form::label('user_type', 'Role') !!}
            {!! Form::select('user_type',[
                    'manager' => "Manager",
                    'staff' => "Staff"]
                    ,null,['class' => 'form-control', 'onchange' => 'onChangeRole()']) !!}
        </div>

        <div class="form-group col-sm-6" id="hotel_grp">
            {!! Form::label('hotel_id', 'Hotel') !!}
            {!! Form::select('hotel_id',$hotels,null,['class' => 'form-control']) !!}
        </div>
    @endif
@endif

