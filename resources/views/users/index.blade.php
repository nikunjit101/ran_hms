@extends('layouts.app')

@section('content')

    <div class="content">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{\Illuminate\Support\Facades\Auth::user()->user_type == 'admin'? 'Users' : 'Staff'}}</h3>
                <a class="btn-sm btn-primary float-right"
                   href="{{ route('users.create') }}">
                    <i class="fas fa-plus"></i>
                </a>
            </div>
            <div class="card-body">
                @include('users.table')
            </div>

        </div>
    </div>

@endsection

