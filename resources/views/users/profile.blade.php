@extends('layouts.app')

@section('content')
    <div class="content-wrapper mrgForCust">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">

                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">{{$user->name}}</h3>
                            </div>

                            <div class="card-body">
                                <strong><i class="fas fa-map-marker-alt mr-1"></i> Address </strong>

                                <p class="text-muted">{{$user->line_1}}, {{$user->line_2}}, {{$user->city}} {{$user->zip}}, {{$user->state}}, {{$user->country}}</p>

                                <hr>

                                <strong><i class="fas fa-at"></i> Email</strong>

                                <p class="text-muted">{{$user->email}}</p>

                                <hr>

                                <strong><i class="fas fa-mobile"></i> Phone</strong>
                                <p class="text-muted">{{$user->contact_no}}</p>

                                <hr>

                                <strong><i class="fas fa-award"></i> Rewards Points</strong>
                                <p class="text-muted">{{$user->reward_point}}</p>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Update Profile</h3>
                                <div>
                                    <a class="btn-sm btn-default float-right"
                                       href="{{ route('bookings.index') }}">
                                        Back
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane active">
                                        @include('adminlte-templates::common.errors')
                                        {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch']) !!}
                                            <div class="form-group row">
                                                {!! Form::label('f_name', 'First Name', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::text('f_name', $user->f_name, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('l_name', 'Last Name', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::text('l_name', $user->l_name, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            {{ Form::hidden('email', $user->email) }}
{{--                                            <div class="form-group row">--}}
{{--                                                {!! Form::label('email', 'Email', ['class' => 'col-sm-2 col-form-label']) !!}--}}
{{--                                                <div class="col-sm-10">--}}
{{--                                                    {!! Form::text('email', $user->email, ['class' => 'form-control']) !!}--}}
{{--                                                </div>--}}
{{--                                            </div>--}}

                                            <div class="form-group row">
                                                {!! Form::label('contact_no', 'Contact No', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::text('contact_no', $user->contact_no, ['class' => 'form-control','maxLength' => 10]) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('id_proof', 'Id Proof', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::text('id_proof', $user->id_proof, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('credit_card', 'Credit Card', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::text('credit_card', $user->credit_card, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('line_1', 'Address 1', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::text('line_1', $user->line_1, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('line_2', 'Address 2', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::text('line_2', $user->line_2, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('city', 'City', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::text('city', $user->city, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('state', 'State', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::text('state', $user->state, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('zip', 'Zip Code', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::number('zip', $user->zip, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('country', 'Country', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::select('country', $countries,($user->country)?$user->country:null,['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="offset-sm-2 col-sm-10">
                                                    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                                </div>
                                            </div>
                                        {{ Form::hidden('name', $user->name) }}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
