<div class="col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $user->name }}</p>
</div>

<div class="col-sm-6">
    {!! Form::label('f_name', 'First Name:') !!}
    <p>{{ $user->f_name }}</p>
</div>

<div class="col-sm-6">
    {!! Form::label('l_name', 'Last Name:') !!}
    <p>{{ $user->l_name }}</p>
</div>

<div class="col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $user->email }}</p>
</div>

<!-- Password Field -->
{{--<div class="col-sm-12">--}}
{{--    {!! Form::label('password', 'Password:') !!}--}}
{{--    <p>{{ $user->password }}</p>--}}
{{--</div>--}}

<div class="col-sm-6">
    {!! Form::label('contact_no', 'Contact No:') !!}
    <p>{{ $user->contact_no }}</p>
</div>

<div class="col-sm-6">
    {!! Form::label('id_proof', 'Id Proof:') !!}
    <p>{{ $user->id_proof }}</p>
</div>

<div class="col-sm-6">
    {!! Form::label('credit_card', 'Credit Card:') !!}
    <p>{{ $user->credit_card }}</p>
</div>

<div class="col-sm-6">
    {!! Form::label('reward_point', 'Reward Point:') !!}
    <p>{{ $user->reward_point }}</p>
</div>

