<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <title></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.5.6/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('css/slider.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url('css/style.css') }}" />
</head>
<body>

<div class="home-slider owl-carousel js-fullheight">
    <div class="slider-item js-fullheight" style="background-image:url({{ url('images/slider-1.jpeg') }});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
                <div class="col-md-12 ftco-animate">
                    <div class="text w-100 text-center">
                        <h2><span style="color: #ffffff">RAN</span> The Best Hotel to Stay</h2>
                        <h1 class="mb-3">India</h1>
                        @if (Route::has('login'))
                            @auth
                                <a href="{{ url('/home') }}" class="btn btn-warning" style="color: #ffffff; background-color: #fd7e14; border-color: #fd7e14">Book Now</a>
                            @else
                                <a href="{{ url('/login') }}" class="btn btn-warning" style="color: #ffffff; background-color: #fd7e14; border-color: #fd7e14">Book Now</a>

                            @endauth
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="slider-item js-fullheight" style="background-image:url({{ url('images/slider-2.jpeg') }});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
                <div class="col-md-12 ftco-animate">
                    <div class="text w-100 text-center">
                        <h2><span style="color: #ffffff">RAN</span> The Best Hotel to Stay</h2>
                        <h1 class="mb-3">United State</h1>
                        @if (Route::has('login'))
                            @auth
                                <a href="{{ url('/home') }}" class="btn btn-warning" style="color: #ffffff; background-color: #fd7e14; border-color: #fd7e14">Book Now</a>
                            @else
                                <a href="{{ url('/login') }}" class="btn btn-warning" style="color: #ffffff; background-color: #fd7e14; border-color: #fd7e14">Book Now</a>

                            @endauth
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="slider-item js-fullheight" style="background-image:url({{ url('images/slider-3.jpeg') }});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
                <div class="col-md-12 ftco-animate">
                    <div class="text w-100 text-center">
                        <h2><span style="color: #ffffff">RAN</span> The Best Hotel to Stay </h2>
                        <h1 class="mb-3">Singapore</h1>
                        @if (Route::has('login'))
                            @auth
                                <a href="{{ url('/home') }}" class="btn btn-warning" style="color: #ffffff; background-color: #fd7e14; border-color: #fd7e14">Book Now</a>
                            @else
                                <a href="{{ url('/login') }}" class="btn btn-warning" style="color: #ffffff; background-color: #fd7e14; border-color: #fd7e14">Book Now</a>

                            @endauth
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('js/jquery.min.js') }}"></script>
<script src="{{ url('js/popper.js') }}"></script>
<script src="{{ url('js/bootstrap.min.js') }}"></script>
<script src="{{ url('js/slider.min.js') }}"></script>
<script src="{{ url('js/main.js') }}"></script>

</body>
</html>
