<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::resource('users', App\Http\Controllers\UserController::class);


Route::resource('hotelBranches', App\Http\Controllers\HotelBranchController::class);


Route::resource('roomTypes', App\Http\Controllers\RoomTypeController::class);


Route::resource('rooms', App\Http\Controllers\RoomController::class);


Route::resource('taskTypes', App\Http\Controllers\TaskTypeController::class);


Route::resource('tasks', App\Http\Controllers\TaskController::class);


Route::resource('bookings', App\Http\Controllers\BookingController::class);

Route::get('/availablebookroom/{id}/{check_in_date}/{check_out_date}','App\Http\Controllers\BookingController@availablebookroom')->name('availablebookroom');

Route::get('/availablerooms/{check_in_date}/{check_out_date}/{location}','App\Http\Controllers\BookingController@availablerooms')->name('availablerooms');

Route::resource('payments', App\Http\Controllers\PaymentController::class);

Route::get('generatereport/{start_date}/{end_date}/{hotel_id}','App\Http\Controllers\PaymentController@generatereport')->name('generatereport');
